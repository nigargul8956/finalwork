package component;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.IpotekaPage;

public class IpotekaComponent extends AbsBaseComponent {
    public IpotekaComponent (WebDriver driver) {
        super(driver);
    }

    @FindBy (css= "div.pl-lg-3>div>a[href=\"https://abb-bank.az/az/ferdi/kreditler/daxili-ipoteka-krediti#applyForm\"]")

   WebElement daxiliIpotekaSifarisi;
    public IpotekaPage chooseDaxiliIpoteka () {
        js.executeScript("arguments[0].scrollIntoView();", daxiliIpotekaSifarisi);
        js.executeScript("arguments[0].click();", daxiliIpotekaSifarisi);
        return new IpotekaPage(driver);
    }
}
