package component;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Bank247Component extends AbsBaseComponent {
    public Bank247Component(WebDriver driver) {
        super(driver);
    }

    @FindBy (css="span:nth-child(1)>a.color-262626.fw-500.fs-18.fs-lg-22.hover.mt-1.mt-lg-0" )
    WebElement abbMobileBank;
    public Bank247Component chooseMobileBank () {
        abbMobileBank.click();
        return new Bank247Component(driver);
    };
}
