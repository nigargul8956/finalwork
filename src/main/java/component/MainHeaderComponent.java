package component;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Bank247Page;
import pages.IpotekaPage;
import java.util.List;

public class MainHeaderComponent extends AbsBaseComponent {
    public MainHeaderComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#js-header-s3>.pr-4")
    private List<WebElement> listOfButtons;

    private WebElement kartlar = listOfButtons.get(3);

     public void  chooseKartlar() {
        kartlar.click();
    }

    private WebElement bank247Button = listOfButtons.get(7);

    public Bank247Page chooseBank247() {
        bank247Button.click();
        return new Bank247Page(driver);
    }

    private WebElement ipoteka = listOfButtons.get(1);

    public IpotekaPage chooseIpoteka() {
        ipoteka.click();
        return new IpotekaPage(driver);
    }
}
