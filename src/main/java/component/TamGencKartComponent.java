package component;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.TamGencKartPage;

public class TamGencKartComponent extends AbsBaseComponent {
    public TamGencKartComponent(WebDriver driver) {
        super(driver);
    }
    @FindBy(css= "[alt=\"tam-genc\"]")
    private WebElement tamGenc;

    @FindBy (css= ".fs-22.fs-lg-36.fw-600.mb-3.mt-4.mt-lg-0")

    private WebElement h1;

    String h1Text= h1.getText();
    public TamGencKartPage tamGencCard () {
        Assert.assertEquals(h1Text,"TamGənc VISA Debet", "Card is not selected right!");
        return new TamGencKartPage(driver);
    }
}
