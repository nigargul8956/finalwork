package component;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.IpotekaMuracietPage;

public class IpotekaMuracietComponent extends AbsBaseComponent {
    public IpotekaMuracietComponent (WebDriver driver)
    { super (driver);}
    @FindBy (css="label[for=from_abb_no]")
    private WebElement xeyr;
    public IpotekaMuracietPage chooseXeyrforEmekHaqqi () {
        xeyr.click();
        Assert.assertTrue(xeyr.isEnabled(), "Button is not clicked");
        return new IpotekaMuracietPage(driver);
    }
}
