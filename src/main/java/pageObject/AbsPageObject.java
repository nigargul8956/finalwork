package pageObject;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;

    public abstract class AbsPageObject {

        protected WebDriver driver;
        protected Actions actions;
        protected JavascriptExecutor js;
        protected WebDriverWait wait;


        public AbsPageObject(WebDriver driver) {
            this.js = js;
            this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            this.driver = driver;
            this.actions = new Actions(driver);
            this.driver.manage().window().maximize();
            this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

            PageFactory.initElements(driver, this);
        }

        public void titleOfMainPage() {
        String title = driver.getTitle();
        Assert.assertEquals(title, "ABB - Müasir, Faydalı, Universal", "Title not correct!");

    }

    public void titleOfKartlarPage () {
       String title2 = driver.getTitle();
        Assert.assertEquals(title2, "Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları", "Title not correct!");
    }

    public void titleOfBank247Page () {
        String title3 = driver.getTitle();
        Assert.assertEquals(title3, "Bank 24/7", "Title not correct!");
    }

    @FindBy(css= ".fs-22.fs-lg-36.fw-600.color-white.mb-3.mb-lg-0.m-0")
    private WebElement header;
    public void headerOfIpotekaPage () {
        String headerText= header.getText();
        Assert.assertEquals (headerText, "İpoteka", "Test failed.");
  }

        }
