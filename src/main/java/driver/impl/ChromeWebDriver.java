package driver.impl;

import exceptions.DriverNotSupportedException;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
public class ChromeWebDriver implements IDriver {
    @Override
    public WebDriver newDriver() throws DriverNotSupportedException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBrowserVersion("116");
       chromeOptions.addArguments("--no-sandbox");
       chromeOptions.addArguments("--no-first-run");
       chromeOptions.addArguments("--homepage=about:blank");
       chromeOptions.addArguments("--ignore-certificate-errors");
        downloadLocalWebDriver(DriverManagerType.CHROME);

        return new ChromeDriver(chromeOptions);
    }
}

