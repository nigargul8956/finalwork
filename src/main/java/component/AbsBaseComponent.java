package component;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import pageObject.AbsPageObject;

public abstract class AbsBaseComponent extends AbsPageObject {
    JavascriptExecutor js;  
    public AbsBaseComponent (WebDriver driver) {
        super (driver);
       js = (JavascriptExecutor) driver;  
    }

    }


