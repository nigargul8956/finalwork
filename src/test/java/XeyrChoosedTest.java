import component.IpotekaComponent;
import component.IpotekaMuracietComponent;
import component.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.IpotekaMuracietPage;
import pages.MainPage;

public class XeyrChoosedTest {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void checkXeyr () {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseIpoteka()
                .headerOfIpotekaPage();
        new IpotekaComponent(driver)
                .chooseDaxiliIpoteka();
        new IpotekaMuracietPage(driver)
                .open("/az/ferdi/kreditler/daxili-ipoteka-krediti#applyForm");
        new IpotekaMuracietComponent(driver)
                .chooseXeyrforEmekHaqqi();
    }
        @AfterMethod
        public void close () {
            if (this.driver != null) {
                this.driver.close();
                this.driver.quit();
            }
        }
    }
