import component.Bank247Component;
import component.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ABBMobileBankPage;
import pages.MainPage;

public class CheckHeaderTest {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void checkHeader () {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseBank247()
                .titleOfBank247Page();
        new Bank247Page(driver)
                .open("/az/ferdi/bank-24-7");
        new Bank247Component(driver)
                .chooseMobileBank();
        new ABBMobileBankPage(driver)
                .open("/az/ferdi/bank-24-7/abb-mobile");
                new ABBMobileBankPage(driver)
                .newWindow();
    }
    @AfterMethod
    public void close () {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
