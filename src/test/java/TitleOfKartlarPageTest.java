import component.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObject.AbsPageObject;
import pages.KartlarPage;
import pages.MainPage;

public class TitleOfKartlarPageTest {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void titleOfKartlar () {
        new MainPage (driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseKartlar();
        new KartlarPage(driver)
               .open("/az/ferdi/kartlar");
        new KartlarPage(driver)
                .titleOfKartlarPage();
     }

    @AfterMethod
    public void close () {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
