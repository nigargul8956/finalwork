package component;

import data.CardData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.KartlarPage;
import java.util.List;

public class KartlarComponent extends AbsBaseComponent {
    public KartlarComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy (css="div>a>div>figure")
    private List<WebElement> listOfCards;

    @FindBy (css="section>div>div>a[href=\"                                                        https://abb-bank.az/az/ferdi/kartlar/tamkart-debet-kartlari/tamgenc-debet \"]>div>h3.m-0.fw-500.fs-18.mt-2.pt-1.color-262626")
    private WebElement tamGencName;

    public KartlarPage chooseCardByName(CardData data) {
        js.executeScript("arguments[0].scrollIntoView();", tamGencName);
        for (WebElement card: listOfCards) {
            if (card.getText().equalsIgnoreCase(data.getName())) {
                card.click();
                Assert.assertEquals(data.getName(),tamGencName.getText());
            break;
            }
        }
        return new KartlarPage(driver);
    }
}

