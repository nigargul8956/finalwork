import component.KartlarComponent;
import component.MainHeaderComponent;
import component.TamGencKartComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.KartlarPage;
import pages.MainPage;
import pages.TamGencKartPage;

import static data.CardData.TamGenc;

public class CorrectCardTest {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void correctCard () {

        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseKartlar();
        new KartlarPage(driver)
                .open("/az/ferdi/kartlar");
        new KartlarPage(driver)
                .titleOfKartlarPage();
        new KartlarComponent(driver)
                .chooseCardByName(TamGenc);
        new TamGencKartPage(driver)
                .open("/az/ferdi/kartlar/tamkart-debet-kartlari/tamgenc-debet");
        new TamGencKartComponent(driver)
                .tamGencCard();
    }
    @AfterMethod
    public void close () {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}

